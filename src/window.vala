[GtkTemplate (ui = "/org/example/TabOverview/window.ui")]
public class TabsMobile.Window : Adw.ApplicationWindow {
    static int next_tab = 1;

    [GtkChild]
    private unowned TabOverview overview;
    [GtkChild]
    private unowned Adw.TabBar tab_bar;
    [GtkChild]
    private unowned Adw.TabView view;
    [GtkChild]
    private unowned Adw.Flap flap;

    private Adw.TabPage? menu_page;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static string[] icon_names = null;

    private Adw.TabPage? get_current_page () {
        return menu_page ?? view.selected_page;
    }

    construct {
        tab_bar.setup_extra_drop_target (Gdk.DragAction.COPY, { typeof (string) });
    }

    private Icon get_indicator_icon (Adw.TabPage page) {
        var muted = page.get_data<bool> ("adw-tab-view-demo-muted");

        if (muted)
            return new ThemedIcon ("tab-audio-muted-symbolic");
        else
            return new ThemedIcon ("tab-audio-playing-symbolic");
    }

    /* These exist for actions only */
    public bool needs_attention {
        get {
            var page = get_current_page ();
            return page == null ? false : page.needs_attention;
        }
        set {
            var page = get_current_page ();
            if (page != null)
                page.needs_attention = value;
        }
    }

    public bool loading {
        get {
            var page = get_current_page ();
            return page == null ? false : page.loading;
        }
        set {
            var page = get_current_page ();
            if (page != null)
                page.loading = value;
        }
    }

    public bool indicator {
        get {
            var page = get_current_page ();
            return page == null ? false : page.indicator_icon != null;
        }
        set {
            var page = get_current_page ();
            if (page != null)
                page.indicator_icon = value ? get_indicator_icon (page) : null;
        }
    }

    public bool icon {
        get {
            var page = get_current_page ();
            return page == null ? false : page.icon != null;
        }
        set {
            var page = get_current_page ();
            if (page != null)
                page.icon = value ? get_random_icon () : null;
        }
    }

    static construct {
        typeof (TabsButton).ensure ();

        install_action ("win.window-new", null, widget => {
            var self = widget as Window;

            var window = new Window (self.application);
            window.prepopulate ();
            window.present ();
        });

        install_action ("win.tab-new", null, widget => {
            var self = widget as Window;

            self.tab_new (true);
        });

        install_action ("win.tab-overview", null, widget => {
            var self = widget as Window;

            if (self.flap.folded && self.flap.reveal_flap)
                return;

            self.overview.is_open = true;
        });

        install_action ("tab.pin", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            self.view.set_page_pinned (page, true);
        });

        install_action ("tab.unpin", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            self.view.set_page_pinned (page, false);
        });

        install_action ("tab.close", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            self.view.close_page (page);
        });

        install_action ("tab.close-other", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            self.view.close_other_pages (page);
        });

        install_action ("tab.close-before", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            self.view.close_pages_before (page);
        });

        install_action ("tab.close-after", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            self.view.close_pages_after (page);
        });

        install_action ("tab.move-to-new-window", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            var window = new Window (self.application);
            self.view.transfer_page (page, window.view, 0);

            window.present ();
        });

        install_property_action ("tab.needs-attention", "needs-attention");
        install_property_action ("tab.loading", "loading");
        install_property_action ("tab.indicator", "indicator");
        install_property_action ("tab.icon", "icon");

        install_action ("tab.refresh-icon", null, widget => {
            var self = widget as Window;
            var page = self.get_current_page ();

            page.icon = self.get_random_icon ();
        });

        install_action ("tab.duplicate", null, widget => {
            var self = widget as Window;
            var parent = self.get_current_page ();

            var page = self.add_page (parent, parent.title, parent.icon);

            page.indicator_icon = parent.indicator_icon;
            page.loading = parent.loading;
            page.needs_attention = parent.needs_attention;

            var data = parent.get_data<bool> ("adw-tab-view-demo-muted");
            page.set_data<bool> ("adw-tab-view-demo-muted", data);

            self.view.selected_page = page;
            page.child.grab_focus ();
        });

        add_binding_action (Gdk.Key.t, Gdk.ModifierType.CONTROL_MASK, "win.tab-new", null);
        add_binding_action (Gdk.Key.n, Gdk.ModifierType.CONTROL_MASK, "win.window-new", null);
        add_binding_action (Gdk.Key.o, Gdk.ModifierType.CONTROL_MASK, "win.tab-overview", null);
        add_binding_action (Gdk.Key.w, Gdk.ModifierType.CONTROL_MASK, "tab.close", null);
    }

    [GtkCallback]
    private void page_detached_cb () {
        if (view.n_pages == 0)
            close ();
    }

    [GtkCallback]
    private void setup_menu_cb (Adw.TabPage? page) {
        menu_page = page;


        Adw.TabPage prev = null;
        uint n_pages = view.n_pages;
        bool pinned = false;
        bool prev_pinned = false;
        bool can_close_before = false;
        bool can_close_after = false;
        bool has_icon = false;

        if (page != null) {
            int pos = view.get_page_position (page);

            if (pos > 0)
                prev = view.get_nth_page (pos - 1);

            pinned = page.pinned;
            prev_pinned = prev != null && prev.pinned;

            can_close_before = !pinned && prev != null && !prev_pinned;
            can_close_after = pos < n_pages - 1;

            has_icon = page.icon != null;
        }

        notify_property ("needs-attention");
        notify_property ("loading");
        notify_property ("indicator");
        notify_property ("icon");

        action_set_enabled ("tab.pin", page == null || !pinned);
        action_set_enabled ("tab.unpin", page == null || pinned);
        action_set_enabled ("tab.close", page == null || !pinned);
        action_set_enabled ("tab.close-before", can_close_before);
        action_set_enabled ("tab.close-after", can_close_after);
        action_set_enabled ("tab.close-other", can_close_before || can_close_after);
        action_set_enabled ("tab.move-to-new-window", page == null || (!pinned && n_pages > 1));
        action_set_enabled ("tab.refresh-icon", has_icon);
    }

    [GtkCallback]
    private unowned Adw.TabView? create_window_cb () {
        var window = new Window (application);

        window.present ();

        return window.view;
    }

    [GtkCallback]
    private Adw.TabPage create_page_cb () {
        return tab_new (false);
    }

    [GtkCallback]
    private bool extra_drag_drop_cb (Adw.TabPage page, Value value) {
        page.title = value.get_string ();

        return true;
    }

    [GtkCallback]
    private void indicator_activated_cb (Adw.TabPage page) {
        var muted = page.get_data<bool> ("adw-tab-view-demo-muted");

        page.set_data<bool> ("adw-tab-view-demo-muted", !muted);

        page.indicator_icon = get_indicator_icon (page);
    }

    public void prepopulate () {
        tab_new (false);
        tab_new (false);
        tab_new (false);
    }

    private void init_icon_names () {
        if (icon_names != null)
            return;

        var display = get_display ();
        var theme = Gtk.IconTheme.get_for_display (display);

        icon_names = theme.get_icon_names ();
    }

    private Icon get_random_icon () {
        init_icon_names ();

        int index = Random.int_range (0, icon_names.length);

        return new ThemedIcon (icon_names[index]);
    }

    private bool text_to_tooltip (Binding binding, Value input, ref Value output) {
        var title = input.get_string ();
        var tooltip = Markup.printf_escaped ("An elaborate tooltip for <b>%s</b>", title);

        output.take_string (tooltip);

        return true;
    }

    private Adw.TabPage add_page (Adw.TabPage? parent, string? title, Icon? icon) {
        var content = new Adw.StatusPage ();
        content.icon_name = "tab-new-symbolic";

        var entry = new Gtk.Entry ();
        entry.halign = Gtk.Align.CENTER;
        entry.text = title;
        content.child = entry;

        var page = view.add_page (content, parent);

        content.bind_property (
            "title", page, "title",
            BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL
        );
        entry.bind_property (
            "text", page, "title",
            BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL
        );
        entry.bind_property (
            "text", page, "tooltip",
            BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL,
            text_to_tooltip
        );

        page.icon = icon;
        page.indicator_activatable = true;

        return page;
    }

    private Adw.TabPage tab_new (bool select) {
        var page = add_page (null, @"Tab $(next_tab)", get_random_icon ());

        if (select) {
            view.selected_page = page;
            page.child.grab_focus ();
        }

        next_tab++;

        return page;
    }
}
