
int main (string[] args) {
	var app = new Adw.Application ("org.example.TabOverview", ApplicationFlags.FLAGS_NONE);

	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new TabsMobile.Window (app);
			((TabsMobile.Window) win).prepopulate ();
		}
		win.present ();
	});

	return app.run (args);
}
