public class TabsMobile.SpringAnimation : GLib.Object {
    private const double DELTA = 0.001;

    public signal void done ();
    public double value { get; private set; }
    public double estimated_duration { get; private set; }

    private Gtk.Widget widget;
    private double value_from;
    private double value_to;
    private double velocity;
    private double damping;
    private double mass;
    private double stiffness;
    private double epsilon;

    private int64 start_time; // ms
    private uint tick_cb_id;

    public SpringAnimation (Gtk.Widget widget, double from, double to, double velocity, double damping, double mass, double stiffness, double epsilon) {
        this.widget = widget;
        value_from = from;
        value_to = to;
        this.velocity = velocity / (to - from);
        this.damping = damping;
        this.mass = mass;
        this.stiffness = stiffness;
        this.epsilon = epsilon;

        value = from;

        estimated_duration = estimate_duration ();
    }

    public SpringAnimation.with_damping_ratio (Gtk.Widget widget, double from, double to, double velocity, double damping_ratio, double mass, double stiffness, double epsilon) {
        double critical_damping = 2 * Math.sqrt (mass * stiffness);
        double damping = damping_ratio * critical_damping;

        this (widget, from, to, velocity, damping, mass, stiffness, epsilon);
    }

    public void stop () {
        if (tick_cb_id == 0)
            return;

        widget.remove_tick_callback (tick_cb_id);
        tick_cb_id = 0;

        widget.unmap.disconnect (stop);

        done ();
    }

    public void start () {
        if (!Adw.get_enable_animations (widget) ||
            !widget.get_mapped () ||
            (value_from - value_to).abs () < epsilon) {
            value = value_to;

            done ();

            return;
        }

        start_time = widget.get_frame_clock ().get_frame_time () / 1000;

        if (tick_cb_id != 0)
            return;

        widget.unmap.connect (stop);

        tick_cb_id = widget.add_tick_callback (tick_cb);
    }

    private bool tick_cb (Gtk.Widget widget, Gdk.FrameClock frame_clock) {
        int64 frame_time = frame_clock.get_frame_time () / 1000;
        double t = (double) (frame_time - start_time) / 1000;

        if (t >= estimated_duration) {
            tick_cb_id = 0;

            value = value_to;

            widget.unmap.disconnect (stop);

            done ();

            return Source.REMOVE;
        }

        value = lerp (value_from, value_to, oscillate (t));

        return Source.CONTINUE;
    }

    // Based on RBBSpringAnimation from RBBAnimation, MIT license.
    // https://github.com/robb/RBBAnimation/blob/master/RBBAnimation/RBBSpringAnimation.m
    private double oscillate (double t) {
        double b = damping;
        double m = mass;
        double k = stiffness;
        double v0 = velocity;

        double beta = b / (2 * m);
        double omega0 = Math.sqrt (k / m);
        double omega1 = Math.sqrt ((omega0 * omega0) - (beta * beta));
        double omega2 = Math.sqrt ((beta * beta) - (omega0 * omega0));

        double x0 = -1;

        double envelope = Math.exp (-beta * t);

        if (beta < omega0) /* Underdamped */
            return -x0 + envelope * (x0 * Math.cos (omega1 * t) + ((beta * x0 + v0) / omega1) * Math.sin (omega1 * t));
        else if (beta == omega0) /* Critically damped */
            return -x0 + envelope * (x0 + (beta * x0 + v0) * t);
        else // Overdamped
            return -x0 + envelope * (x0 * Math.cosh (omega2 * t) + ((beta * x0 + v0) / omega2) * Math.sinh (omega2 * t));
    }

    private double estimate_duration () {
        double beta = damping / (2 * mass);

        if (beta <= 0)
            return double.INFINITY;

        double omega0 = Math.sqrt (stiffness / mass);
        double x0 = -Math.log (epsilon) / beta;

        if (beta <= omega0)
            return x0;

        double y0 = oscillate (x0);
        double m = (oscillate (x0 + DELTA) - y0) / DELTA;

        double x1 = (1 - y0 + m * x0) / m;
        double y1 = oscillate (x1);

        while ((1 - y1).abs () > epsilon) {
            x0 = x1;
            y0 = y1;

            m = (oscillate (x0 + DELTA) - y0) / DELTA;

            x1 = (1 - y0 + m * x0) / m;
            y1 = oscillate (x1);
        }

        return x1;
    }
}

public inline double lerp (double a, double b, double t) {
    return a * (1.0 - t) + b * t;
}

public inline float lerpf (float a, float b, float t) {
    return a * (1.0f - t) + b * t;
}
