[GtkTemplate (ui = "/org/example/TabOverview/tabs-button.ui")]
public class TabsMobile.TabsButton : Gtk.Button {
    // Copied from GtkInspector code
    private const double XFT_DPI_MULTIPLIER = 1.0 / 96.0 / 1024.0;
    private const int FONT_SIZE_LARGE = 8;
    private const int FONT_SIZE_SMALL = 6;

    [GtkChild]
    private unowned Gtk.Label tabs_label;
    [GtkChild]
    private unowned Gtk.Image tabs_icon;

    private int _n_tabs;
    public int n_tabs {
        get { return _n_tabs; }
        set {
            if (_n_tabs == value)
                return;

            _n_tabs = value;

            update_icon ();
        }
    }

    construct {
        n_tabs = 1;
    }

    static construct {
        set_layout_manager_type (typeof (Gtk.BinLayout));
    }

    protected override void constructed () {
        update_icon ();

        update_label_scale ();
        get_settings ().notify["gtk-xft-dpi"].connect (update_label_scale);

        base.constructed ();
    }

    // FIXME: I hope there is a better way to prevent label from changing scale
    private void update_label_scale () {
        double xft_dpi = get_settings ().gtk_xft_dpi;

        tabs_label.attributes.change (Pango.attr_scale_new (XFT_DPI_MULTIPLIER * xft_dpi));
    }

    private void update_icon () {
        bool overflow = n_tabs >= 100;
        int font_size = n_tabs >= 10 ? FONT_SIZE_SMALL : FONT_SIZE_LARGE;

        tabs_label.visible = !overflow;
        tabs_label.label = @"$n_tabs";
        tabs_label.attributes.change (new Pango.AttrSize (font_size * Pango.SCALE));
        tabs_icon.icon_name = overflow
            ? "tab-overflow-symbolic"
            : "tab-counter-symbolic";
    }
}
