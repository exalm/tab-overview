public class TabsMobile.Gizmo : Gtk.Widget {
    public Gizmo (string css_name = "widget") {
        Object (css_name: css_name);
    }

    protected override void dispose () {
        var child = get_first_child ();

        while (child != null) {
            var c = child;
            child = child.get_next_sibling ();

            c.unparent ();
        }

        base.dispose ();
    }
}
