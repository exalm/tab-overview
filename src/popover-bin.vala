public class TabsMobile.PopoverBin : Gtk.Widget {
    private Gtk.Widget? _child;
    public Gtk.Widget? child {
        get { return _child; }
        set {
            if (child == value)
                return;

            if (child != null)
                child.unparent ();

            _child = value;

            if (child != null)
                child.set_parent (this);
        }
    }

    private Gtk.Popover? _popover;
    public Gtk.Popover? popover {
        get { return _popover; }
        set {
            if (popover == value)
                return;

            if (popover != null)
                popover.unparent ();

            _popover = value;

            if (popover != null)
                popover.set_parent (this);
        }
    }

    protected override void dispose () {
        popover = null;
        child = null;

        base.dispose ();
    }

    protected override void measure (Gtk.Orientation orientation, int for_size, out int minimum, out int natural, out int minimum_baseline, out int natural_baseline) {
        minimum = 0;
        natural = 0;
        minimum_baseline = -1;
        natural_baseline = -1;

        if (child != null)
            child.measure (orientation, for_size, out minimum, out natural, out minimum_baseline, out natural_baseline);
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (child != null)
            child.allocate (width, height, baseline, null);

        if (popover != null)
            popover.present ();
    }
}
